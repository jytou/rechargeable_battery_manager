<?php
require "header.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Explore Devices</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<body>
<form action="viewdevice.php" method="post">
<?php
/**
 * SHOW MESSAGE IF NEEDED
 */
if (isset($_GET["message"]))
    echo htmlentities(rawurldecode($_GET["message"]))."<br>";

require_once "connect.php";
$conn = connect();

?>
Choose a device.<br>
<?php
    // No device chosen, offer to choose one first, with either the combo, either by clicking on the device's image
    $sMove = $conn->prepare("select evt_type from evt where device_id=? and evt_type in (2, 3, 4) order by mydate desc limit 1") or die ($conn->error);
    $sDevice = $conn->prepare("select id, shortname from device where dev_type < 2") or die ($conn->error);
    $sDevice->execute();
    $rsDevice = $sDevice->get_result();
    while ($assocDevice = $rsDevice->fetch_assoc())
    {
        $deviceid = $assocDevice["id"];
        /**
         * THEN GET THE LATEST MOVE FOUND FOR THIS DEVICE - SEE IF WE ARE LOADING OR UNLOADING
         */
        $sMove->bind_param("i", $deviceid) or die ($conn->error);
        $sMove->execute();
        $rsMove = $sMove->get_result();
        $assocMove = $rsMove->fetch_assoc();
        $loaded = False;
        if ($assocMove && ($assocMove["evt_type"] == 2))
            $loaded = True;
        $rsMove->close();
        echo "<a href=\"viewdevice.php?devid=$deviceid\"><img src=\"showimg.php?devid=$deviceid\" border=\"3\" style=\"border-color:#".($loaded ? "FF0000" : "00AA00")."\"></a>\n";
    }
    $rsDevice->close();
    $sDevice->close();
    $sMove->close();
    $conn->close();
?>
<p>
<a href="main.php">Back to Menu</a>
</body>
</html>
