<?php
require "header.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Batteries Manager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css?version=001" />
    <link rel="icon" href="icon.png"/>
</head>
<body>
<?php
if (isset($_GET["message"]))
    echo htmlentities(rawurldecode($_GET["message"]))."<br>";
?>
<div class="button"><a href="addmove.php">Load/Unload Batteries</a></div><br>
<div class="button"><a href="addcharge.php?measuring=0">Charge Batteries</a></div><br>
<div class="button"><a href="addcharge.php?measuring=1">Add Measurement</a></div><br>
<div class="button"><a href="addcharge.php?reportE=1">Report Empty Batteries</a></div><br>
<div class="button"><a href="addbatt.php">Add Batteries</a></div><br>
<div class="button"><a href="adddevice.php">Add Device</a></div><br>
<div class="button"><a href="addmake.php">Add Make</a></div><br>
<div class="button"><a href="searchbat.php">Search Batteries</a></div><br>
<div class="button"><a href="expldevices.php">Explore Devices</a></div>
</body>
</html>
