<?php
require "header.php";
require_once "helpers.php";
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>View Device</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="main.css?version=001" />
</head>
<body>
<table>
<?php
$devid = intval($_GET["devid"]);
require_once "connect.php";
$conn = connect();
$s = $conn->prepare("select name, shortname, battype.type as batt_type, nb_batt, dev_type from device, battype where device.id=? and device.type_id=battype.id") or die ($conn->error);

$s->bind_param("i", $devid) or die($conn->error);
$s->execute();
$rs = $s->get_result();
if ($assoc = $rs->fetch_assoc())
{
	$dev_name = $assoc["name"];
	$dev_short = $assoc["shortname"];
	$dev_type = $assoc["dev_type"];
	$batt_type = $assoc["batt_type"];
	$nb_batt = $assoc["nb_batt"];
	echo "<tr><td>Name</td><td>$dev_name</td></tr>\n";
	echo "<tr><td>Short Name</td><td>$dev_short</td></tr>\n";
	echo "<tr><td>Image</td><td><img src=\"showimg.php?devid=$devid\" width=\"100\" height=\"100\"></td></tr>\n";
	echo "<tr><td>Battery Type</td><td>$batt_type</td></tr>\n";
	echo "<tr><td>Nb Batteries</td><td>$nb_batt</td></tr>\n";
	//echo "<tr><td>Device Type</td><td>$dev_type</td></tr>\n";
	echo "</tr></table><br><table border=1><tr><td>Event</td><td>Date</td><td>Interval</td><td>Batteries</td></tr>";
	$sEvents = $conn->prepare("select mydate, batt_id, num, evt_type from evt, battery where batt_id=battery.id and device_id=? order by mydate desc, evt_type") or die($conn->error);
	$sEvents->bind_param("i", $devid) or die($conn->error);
	$sEvents->execute();
	$rsEvents = $sEvents->get_result();
	$lastdate = null;
	while ($assocEvents = $rsEvents->fetch_assoc())
	{
		$curdate = new DateTime($assocEvents["mydate"]);
		switch ($assocEvents["evt_type"])
		{
			case 0:
				$event = "Charged";
				break;
			case 1:
				$event = "Measured";
				break;
			case 2:
				$event = "Load";
				break;
			case 3:
			case 4:
				$event = "Unload";
				if ($assocEvents["evt_type"] == 4)
					$event .= " (was empty)";
				break;
			case 6:
				$event = "Measure what is left";
				break;
			case 7:
				$event = "Measure Charge";
				break;
		}
		$batt_num = $assocEvents["num"];
		if ($lastdate == $curdate)
		{
			// Just show the current battery, that's all
			echo ", ";
		}
		else
		{
			if ($lastdate != null)
				echo "</td></tr>";
			echo "<tr><td>$event</td><td>".$assocEvents["mydate"]."</td>";
			if ($lastdate == null)
				$datediff = "";
			else
				$datediff = format_interval($curdate->diff($lastdate));
			$lastdate = $curdate;
			echo "<td>".$datediff."</td>";
			echo "<td>";
		}
		echo "<a href='viewbatt.php?id=".$assocEvents["batt_id"]."'>".$batt_num."</a>";
	}
	if ($lastdate != null)
		echo "</td></tr>";
	$rsEvents->close();
	$sEvents->close();
	echo "</table>\n";
}
$rs->close();
$s->close();
?>
<a href="expldevices.php">Back to devices</a>
</body>
</html>
