<?php
require_once "header.php";
require_once "connect.php";
$conn = connect();
$s = $conn->prepare("select image from batmake where id=?") or die($conn->error);
$makeid = intval($_GET["makeid"]);
$s->bind_param("i", $makeid) or die($conn->error);
$s->execute();
$rs = $s->get_result();
$assoc = $rs->fetch_assoc();
if ($assoc == NULL)
	die("No such type ".$_GET["makeid"]);
$img = $assoc['image'];
$rs->close();
$s->close();
$conn->close();
header('Content-Type: image/jpeg');
echo $img;
?>
