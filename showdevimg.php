<?php
require_once "header.php";
require_once "connect.php";
$conn = connect();
$s = $conn->prepare("select image from device where id=?") or die($conn->error);
$s->bind_param("i", intval($_GET["devid"])) or die($conn->error);
$s->execute() or die($conn->error);
$rs = $s->get_result() or die ($conn->error);
$assoc = $rs->fetch_assoc() or die ($conn->error);
if ($assoc == NULL)
	die("No such device ".$_GET["devid"]);
$img = $assoc['image'];
$rs->close();
$s->close();
$conn->close();
header('Content-Type: image/jpeg');
echo $img;
?>
