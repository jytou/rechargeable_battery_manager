<?php
require "header.php";
require_once "helpers.php";

if ((!isset($_GET["num"])) || (!isset($_GET["type"])))
	die("not enough data");
$batnum = $_GET["num"];
$battype = $_GET["type"];
require_once "connect.php";
$conn = connect();
// Moves to/from devices
$s = $conn->prepare("select e.evt_type, m.id from batmake m, battery b left join evt e on b.id=e.batt_id and e.evt_type in (2, 3, 4) where b.make_id=m.id and b.num=? and m.type_id=? order by mydate desc limit 1") or die ($conn->error);
$s->bind_param("ii", $batnum, $battype);
$s->execute();
$evttype = $maketype = null;
$s->bind_result($evttype, $maketype);
$res = array();
if ($s->fetch())
{
	$res["status"] = "OK";
	if ($evttype == 2)
		$res["batt"] = "TAKEN";
	else
		$res["batt"] = "AVAIL";
	$res["make"] = $maketype;
}
else
	$res["status"] = "KO";
$s->close();
$conn->close();
header('Content-Type: application/json');
echo json_encode($res);
?>
