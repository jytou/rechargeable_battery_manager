<?php
session_start();
if (!isset($_SESSION['USERID']))
{
	if (strpos($_SERVER['REMOTE_ADDR'], "192.168.") == 0)
		$userid = 1;
	else
		header('Location: login.php');
}
else
	$userid = $_SESSION['USERID'];

$batttype = $_GET["batttype"];
$battnums = $_GET["battnums"];

require_once 'connect.php';
$res = array();
$res["bats"] = "<table border=0>";
$conn = connect();
$s = $conn->prepare("select e.mydate, e.mah, e.evt_type, m.nominal, b.make_id from evt e join battery b on e.batt_id=b.id and b.num=? join batmake m on b.make_id=m.id and m.type_id=? order by e.mydate desc") or die("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");

function showGaugeCircle($angle, $bgColor, $fgColor, $size = 20, $border = 1, $title = null)
{
	$ray = $size / 2;
	$inRay = $size / 2 - $border;
	$res = "<span".($title !== null ? " title='".str_replace("'", "\\'", $title)."'" : "")."><svg width=$size height=$size".($title !== null ? " title='".str_replace("'", "\\'", $title)."'" : "")."><circle cx=$ray cy=$ray r=$inRay stroke='none' fill='#$bgColor'></circle>";
	if ($angle > 0)
	{
		$res .= "<path d=\"M $ray $ray L $ray $border ";
		if ($angle > 360)
			$angle = 360;
		for ($a = 10; $a <= $angle; $a += 10)
			$res .= "L ".($ray + round($inRay * sin($a / 180 * M_PI)))." ".($ray - round($inRay * cos($a / 180 * M_PI)));
		$res .= "z\" style=\"stroke:none;fill:#$fgColor\"/>";
	}
	$res .= "<circle cx=$ray cy=$ray r=$inRay stroke='#000' stroke-width=1 fill='none'></circle>";
	$res .= "</svg></span>";
	return $res;
}

function fillForBatt($num)
{
	global $conn;
	global $res;
	global $s;
	global $batttype;
	$s->bind_param("ii", $num, $batttype);
	$s->execute() or die ("{\"error\":\"".$conn->error."\",\"status\":\"KO\"}");
	$rsDte = $rsMah = $rsEvtType = $rsNominal = $rsMakeId = null;
	$s->bind_result($rsDte, $rsMah, $rsEvtType, $rsNominal, $rsMakeId);
	$load = null;
	$res["bats"] .= "<tr><td><strong>$num</strong>: ";
	while ($s->fetch())
	{
		if ($load === null)
		{
			if ($rsEvtType === 2)
			{
				$load = $rsEvtType;
				$res["bats"] .= "<font color=#f00>ERROR LOADED</font> ";
			}
			else if (($rsEvtType === 3) || ($rsEvtType === 4))
				$load = $rsEvtType;
		}
		if ((($rsEvtType === 0) || ($rsEvtType === 1)) && ($rsMah !== null))
		{
			if ($rsNominal !== null)
				$res["bats"] .= showGaugeCircle(min(360, 360.0 * $rsMah / $rsNominal), "f00", "0f0", 20, 1, "$rsMah mAh / $rsNominal\n$rsDte")." ";
			else
			// no nominal value, we can't do much
				$res["bats"] .= "$rsMah ";
		}
	}
	if ($rsMakeId !== null)
		$res["bats"] .= "<img src=\"showmakeimg.php?makeid=$rsMakeId\" height=16>";
	$res["bats"] .= "</td></tr>";
}

foreach (explode(",", $battnums) as $battnumdash)
	if ($battnumdash != "")
	{
		if (strpos($battnumdash, "-") === false)
			fillForBatt($battnumdash);
		else
		{
			$first = intval(substr($battnumdash, 0, strpos($battnumdash, "-")));
			$last = intval(substr($battnumdash, strpos($battnumdash, "-") + 1));
			for ($i = $first; $i <= $last; $i++)
				fillForBatt($i);
		}
	}

$res["bats"] .= "</table>";
$s->close();
$conn->close();
$res["status"] = "OK";
header('Content-Type: application/json');
echo json_encode($res);
?>