<?php
require "header.php";
$measuring = (isset($_GET["measuring"]) && ($_GET["measuring"] == "1"));
$reportEmpty = (isset($_GET["reportE"]) && ($_GET["reportE"] == "1"));
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $measuring ? "Add Measurement on Battery" : "Add Battery Charge";?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
</head>
<body>
<?php
if (isset($_GET["message"]))
	echo htmlentities(rawurldecode($_GET["message"]))."<br>";
?>
<form action="doaddcharge.php" method="post">
<?php
if ($reportEmpty)
	echo "<font color=\"#FF0000\">Reporting empty batteries and charging them</font><br>\n";
echo "<input type=\"hidden\" name=\"measuring\" value=\"".($measuring ? "1" : "0")."\">";
echo "<input type=\"hidden\" name=\"reporting\" value=\"".($reportEmpty ? "1" : "0")."\">";
?>
<script type="text/javascript">
var lastTyped = 0;
var isLooping = false;
var isFetching = false;

function getBattInfo()
{
	isFetching = true;
// 	document.getElementById("battFetching").innerHTML = "Fetching...";
	var xhr = new XMLHttpRequest();
	var batttypeselect = document.getElementById("batTypeSelect");
	var batttype = batttypeselect.options[batttypeselect.selectedIndex].value;
	var battnums = document.getElementById("batNumsText").value;
	xhr.open('GET', 'getlastmeas.php?batttype=' + batttype + '&battnums=' + battnums);
	xhr.onload = function()
	{
		isFetching = false;
// 		document.getElementById("battFetching").innerHTML = "";
		if (xhr.status != 200) // analyze HTTP status of the response
			alert("error");
		else
		{
			try {
				var r = JSON.parse(xhr.response);
				if (r.status == "OK")
				{
					document.getElementById("battInfoTd").innerHTML = r.bats;
				}
				else
					alert("Error: " + r["error"]);
			}
			catch (e) {
				alert("Error while parsing response: " + xhr.response + ", " + e);
			}
		}
	};
	xhr.onerror = function()
	{
		isFetching = false;
// 		document.getElementById("battFetching").innerHTML = "Error";
		alert("error");
	};
	xhr.send();
}

function researchBatt()
{
// 	document.getElementById("battLooping").innerHTML = "Looping...";
	if ((isFetching) || (Date.now() < lastTyped + 1000))
	{
// 		document.getElementById("battLooping").innerHTML = "Waiting...";
		setTimeout(researchBatt, 100);
		return;
	}
	else
	{
		isLooping = false;
		getBattInfo();
// 		document.getElementById("battLooping").innerHTML = "";
	}
}

function batNumTyped()
{
// 	document.getElementById("battTyping").innerHTML = "Typing...";
	lastTyped = Date.now();
	if (!isLooping)
	{
		isLooping = true;
		setTimeout(researchBatt, 100);
	}
	//document.getElementById("battTyping").innerHTML = "";
}
</script>
<table>
<tr><td>Type</td><td><select name="type" id="batTypeSelect">
<?php
require_once "connect.php";
require_once "helpers.php";
$conn = connect();
showBatteryTypeOptions($conn);
?>
</select></td></tr>
<tr><td>Battery Number<?php echo $measuring ? "" : "s<br>ex: 32-34,40,44-48"; ?></td><td><input type="text" name="battnum" id="batNumsText" oninput="javascript:batNumTyped();return false;"></td></tr>
<tr><td></td><td id="battInfoTd"></td></tr>
<tr><td></td><td id="battTyping"></td></tr>
<tr><td></td><td id="battLooping"></td></tr>
<tr><td></td><td id="battFetching"></td></tr>
<tr><td><?php echo $measuring ? "Measuring Device" : "Charging Device";?></td><td><select name="charging_device">
<?php
showChargingDeviceOptions($conn, $measuring);
$conn->close();
?>
</select></td></tr>
<?php
if ($measuring)
{
?>
<tr><td>Measurement</td><td><input type="text" name="measure"></td></tr>
<tr><td>Measurement Type</td><td>
<select name="measure_type">
<option value="1">Full capacity</option>
<option value="6">Left after unloading</option>
<option value="7">Charged after unloading</option>
</select>
</td></tr>
<tr><td>Charged at end of Measurement?</td><td><input type="checkbox" name="recharged" value="No" checked></td></tr>
<?php
}
?>
<tr><td>Date <?php echo $measuring ? "Measured" : "Charged";?></td><td><?php echo "<input type=\"text\" name=\"mydate\" value=\"".date("Y-m-d H:i:s")."\">"; ?></td></tr>
<tr><td>Charge measure (full charge)</td><td><input type="text" name="charge_amount"></td></tr>
</table>
<input type="submit">
</form>
<p>
<a href="main.php">Back to Menu</a>
</body>
</html>
